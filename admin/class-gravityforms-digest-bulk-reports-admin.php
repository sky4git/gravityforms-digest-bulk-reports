<?php
namespace DIGESTREPORTS;
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://me@example.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/admin
 * @author     Aakash <me@example.com>
 */
class Gravityforms_Digest_Bulk_Reports_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravityforms_Digest_Bulk_Reports_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravityforms_Digest_Bulk_Reports_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gravityforms-digest-bulk-reports-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravityforms_Digest_Bulk_Reports_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravityforms_Digest_Bulk_Reports_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gravityforms-digest-bulk-reports-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	* admin_init hook function
	* 
	* @return void data
	*/
	public function  admin_init( ){

		// assign admin role a capability
		$this->_assign_capability();

		if ( !isset( $_GET['page'] ) || $_GET['page'] != 'gf_edit_forms' )
				return; // Nothing else to do, we're not on the setting page

		// if post save exist in request
		if ( isset( $_POST['save'] ) ){
				$this->_process_post_request();
		}
	}//end func

	/**
	 *  Parse save data 
	 */
	private function _process_post_request() {
		
		if ( ! \current_user_can( 'manage_digest_bulk_reports' ) ) {
			\wp_die( __('Cheatin&#8217; uh?') );
		}

		if ( !isset( $_POST['form_notification_digest_screen'] ) )
			return; // Wrong screen

		$form_id = isset( $_GET['id'] ) ? sanitize_text_field($_GET['id']) : null;
		if ( !$form_id ) return; // Not supposed to be here

		$form = \RGFormsModel::get_form_meta( $form_id );
		if ( !$form ) return; // Nuh-uh

		/* Process the settings bit by bit */
		if ( !isset( $_POST['form_notification_enable_digest'] ) ) {
			$form['digests']['enable_digest'] = false;

			if ( version_compare( \GFCommon::$version, '1.7' ) >= 0 ) {
				/* Seems like 1.7 really messed up the meta structure */
				unset( $form['notifications'] );
				unset( $form['confirmations'] );
			}
			\RGFormsModel::update_form_meta( $form_id, $form );
			if ( version_compare( \GFCommon::$version, '1.7' ) >= 0 ) {
				/* In 1.7 there seems to be an issue with saving */
				\GFFormsModel::flush_current_forms();
			}

			$this->_reschedule_existing();

			return; // Nothing of interest here, move on
		}

		// TODO: This has to be a function of its own, the tests need it

		$digest_emails = isset( $_POST['form_notification_digest_emails'] ) ?  $_POST['form_notification_digest_emails']  : '';
		$digest_interval = isset( $_POST['form_notification_digest_interval'] ) ?  $_POST['form_notification_digest_interval']  : '';
		$digest_group = isset( $_POST['form_notification_digest_group'] ) ?  $_POST['form_notification_digest_group']  : '';
		$digest_report_always = isset( $_POST['form_notification_digest_report_always'] ) ?  $_POST['form_notification_digest_report_always']  : '';
		$digest_export_all_fields = ( $_POST['form_notification_digest_export_fields'] == 'specified' ) ? false : true;
		$digest_export_field_list = array();

		if ( !$digest_export_all_fields ) {
			foreach ( $form['fields'] as $field ) {
				if ( isset( $_POST['form_notification_digest_field_' . $field['id']] ) && $_POST['form_notification_digest_field_' . $field['id']] ) {
					$digest_export_field_list[] = $field['id'];
				}
			}
		}

		$form['digests']['enable_digest'] = true;
		$form['digests']['digest_emails'] = array_map( 'trim', explode( ',', $digest_emails ) );

		$form['digests']['digest_interval'] = $digest_interval;
		$form['digests']['digest_group'] = $digest_group;
		$form['digests']['digest_report_always'] = $digest_report_always;
		$form['digests']['digest_export_all_fields'] = $digest_export_all_fields;
		$form['digests']['digest_export_field_list'] = $digest_export_field_list;

		if ( version_compare( \GFCommon::$version, '1.7' ) >= 0 ) {
			/* Seems like 1.7 really messed up the meta structure */
			unset( $form['notifications'] );
			unset( $form['confirmations'] );
		}
		\RGFormsModel::update_form_meta( $form_id, $form );

		if ( version_compare( \GFCommon::$version, '1.7' ) >= 0 ) {
			/* In 1.7 there seems to be an issue with saving */
			\GFFormsModel::flush_current_forms();
		}

		$this->_reschedule_existing();
	}// end func

	/**
	* Reschedule existing events
	* 
	* @return void data
	*/
	private function _reschedule_existing( ){

		$groups = array();
		foreach( \RGFormsModel::get_forms( true ) as $existing_form ) {
			$existing_form = \RGFormsModel::get_form_meta( $existing_form->id );

			if ( !isset( $existing_form['digests'] ) )
				continue;
			if ( !isset( $existing_form['digests']['enable_digest'] ) )
				continue;
			if ( !$existing_form['digests']['enable_digest'] )
				continue;
			if ( !isset( $existing_form['digests']['digest_interval'] ) )
				continue;
			if ( !$existing_form['digests']['digest_interval'] )
				continue;

			$group = isset( $existing_form['digests']['digest_group'] ) ? $existing_form['digests']['digest_group'] : '';
			$interval = $existing_form['digests']['digest_interval'];

			if ( !$group || !isset( $groups["$group.$interval"] ) ) {
				\wp_schedule_event( // Schedule only once
					\apply_filters( 'gf_digest_schedule_next', time() + 3600, $interval ),
					$interval, 'gf_digest_send_notifications', array( intval( $existing_form['id'] ) ) ); 
				$groups["$group.$interval"] = $existing_form['id'];
			}
		}
	
	}//end func

	/**
	 *  Remove all schedules
	 */
	public function remove_schedules() {

		$cron = _get_cron_array();

		foreach ( $cron as $timestamp => $schedule ) {
			if ( !isset( $schedule['gf_digest_send_notifications'] ) )
				continue;

			unset( $cron[$timestamp]['gf_digest_send_notifications'] );

			if ( empty( $cron[$timestamp] ) )
				unset( $cron[$timestamp] );
		}

		_set_cron_array( $cron );
	}// end func


	/**
	 * Early initializtion for admin interface
	 */
	public function plugins_loaded() {
		/* Add a new meta box to the settings; use `gform_notification_ui_settings` in 1.7 */
		if ( version_compare( \GFCommon::$version, '1.7' ) >= 0 ) {
			\add_action( 'gform_form_settings_page_digests', array( $this, 'show_notification_settings' ) );
			\add_filter( 'gform_form_settings_menu', array( $this, 'add_notification_settings_tab' ) );
			return;
		}
		\add_filter( 'gform_save_notification_button', array( $this, 'add_notification_settings' ) );
	}// end func

	/**
	 * Adding a tab in GF 1.7+
	 */
	public function add_notification_settings_tab( $tabs ) {
		$tabs []= array( 'name' => 'digests', 'label' => __( 'Notification Digest', 'gravityforms-digest-bulk-reports' ), 'query' => array( 'nid' => null ) );
		return $tabs;
	}// end func

	/**
	 * This is a GF 1.7+ UI 
	 */
	public function show_notification_settings() {			
		\GFFormSettings::page_header();
		echo '<form method="post">';
		echo $this->add_notification_settings( '' );
		echo '<input type="submit" id="gform_save_settings" name="save" value="Update" class="button-primary gfbutton"></form>';
		\GFFormSettings::page_footer();
	}// end func

	/**
	 * Add an extra screen to the Notification settings of a Form
	 */
	public function add_notification_settings( $out ) {

		$form_id = isset( $_GET['id'] ) ? $_GET['id'] : null;
		if ( !$form_id ) return $out; // Not supposed to be here

		$form = \RGFormsModel::get_form_meta( $form_id );

		$is_digest_enabled = isset( $form['digests']['enable_digest'] ) ?  $form['digests']['enable_digest'] : false;
		$digest_emails = isset( $form['digests']['digest_emails'] ) ?  $form['digests']['digest_emails']  : false;
		$digest_emails = ( $digest_emails ) ? implode( ',', $digest_emails ) : '';
		$digest_interval = isset( $form['digests']['digest_interval'] ) ?  $form['digests']['digest_interval']  : false;
		$digest_group = isset( $form['digests']['digest_group'] ) ?  $form['digests']['digest_group']  : false;
		$digest_report_always = isset( $form['digests']['digest_report_always'] ) ?  $form['digests']['digest_report_always']  : false;
		$digest_export_all_fields = isset( $form['digests']['digest_export_all_fields'] ) ?  $form['digests']['digest_export_all_fields']  : true;
		$digest_export_field_list = isset( $form['digests']['digest_export_field_list'] ) ?  $form['digests']['digest_export_field_list']  : array();

		$next = \wp_next_scheduled( 'gf_digest_send_notifications', array( intval( $form_id ) ) );
		if ( $next ) $next = 'next scheduled in ' . ( $next - time() ) . ' seconds';
		$last = isset( $form['digests']['digest_last_sent'] ) ? $form['digests']['digest_last_sent'] : 0;
		$last = $last ? 'last sent lead ' . $last : '';

		?>
			<div id="submitdiv" class="stuffbox">
				<h3><span class="hndle"><?php _e( 'Notification Digest', 'gravityforms-digest-bulk-reports' ); ?></span></h3>
				<div class="inside" style="padding: 10px;">
					<input type="hidden" name="form_notification_digest_screen" value="true">
					<input type="checkbox" name="form_notification_enable_digest" id="form_notification_enable_digest" value="1" <?php checked( $is_digest_enabled ); ?> onclick="if(this.checked) {jQuery('#form_notification_digest_container').show('slow');} else {jQuery('#form_notification_digest_container').hide('slow');}"/> <label for="form_notification_enable_digest"><?php _e("Enable digest notifications", 'gravityforms-digest-bulk-reports'); ?></label>

					<div id="form_notification_digest_container" style="display:<?php echo $is_digest_enabled ? "block" : "none"?>;">
						<br>
						<label for="form_notification_digest_emails">Digest Addresses<a href="#" onclick="return false;" class="tooltip tooltip_notification_digest_emails" tooltip="<h6>Digest Addresses</h6>Comma-separated list of e-mail addresses that receive notification digests for this form.">(?)</a></label>
						<input class="fieldwidth-1" name="form_notification_digest_emails" id="form_notification_digest_emails" value="<?php echo esc_attr( $digest_emails ); ?>" type="text">
						<br>
						<br>
						<label for="form_notification_digest_interval">Digest Interval<a href="#" onclick="return false;" class="tooltip tooltip_notification_digest_interval" tooltip="<h6>Digest Interval</h6>An interval at which a digest is sent out. More intervals can be added using the WordPress <code>cron_schedules</code> filter.">(?)</a></label>
						<br>
						<select id="form_notification_digest_interval" name="form_notification_digest_interval">
							<?php foreach ( \wp_get_schedules() as $value => $schedule ): ?>
								<option value="<?php echo esc_attr( $value ); ?>" <?php selected( $digest_interval, $value ); ?>><?php echo esc_html( $schedule['display'] ); ?></option>
							<?php endforeach; ?>
						</select>
						<p>Once the interval is changed, the first report will be sent out in an hour and then at the set intervals. This behavior can be changed by hooking into the <code>gf_digest_schedule_next</code> filter. Behavior may vary for forms grouped together, report will be sent out whenever the first or only group was scheduled.</p>
						<label for="form_notification_digest_group">Group<a href="#" onclick="return false;" class="tooltip tooltip_notification_digest_group" tooltip="<h6>Digest Group</h6>We will try and group forms with the same interval into one e-mail, leave blank for no grouping. Can be a number or keyword.">(?)</a></label>
						<input type="text" name="form_notification_digest_group" id="form_notification_digest_group" value="<?php echo esc_attr( $digest_group ); ?>">
						<p>Note that digest grouping will only work for members of a group with same intervals set. For example, forms with hourly digests in group 'sales' will be bound together, daily digests in group 'sales' will be bound together. So if you want to see two form digests in one e-mail set the same interval and the same group for the two forms. You may also receive out of band reports once after having changed groups or intervals.</p>
						<input type="checkbox" name="form_notification_digest_report_always" id="form_notification_digest_report_always" value="1" <?php checked( $digest_report_always ); ?> />
						<label for="form_notification_digest_report_always"><?php _e( 'Generate digest report even if there are no new entries.', 'gravityforms-digest-bulk-reports' ); ?></label>
						<br>
						<br>
						<label for="form_notification_digest_export_fields">Export:</label>
						<br>
						<select id="form_notification_digest_export_fields" name="form_notification_digest_export_fields" onchange="if(jQuery(this).find(':selected').val() === 'specified') {jQuery('#form_notification_digest_fieldlist_container').show('fast');} else {jQuery('#form_notification_digest_fieldlist_container').hide('fast');};">
							<option value="all" <?php selected( $digest_export_all_fields ); ?>>All fields</option>
							<option value="specified" <?php selected( !$digest_export_all_fields ); ?>>Specified fields only</option>
						</select>
						<br>

						<div id="form_notification_digest_fieldlist_container" style="display: <?php echo $digest_export_all_fields ? 'none' : 'block'; ?>;">
							<?php foreach ( $form['fields'] as $field ): ?>
								<input type="checkbox" name="form_notification_digest_field_<?php echo $field['id']; ?>" id="form_notification_digest_field_<?php echo $field['id']; ?>" value="1" <?php checked( in_array( $field['id'], $digest_export_field_list ) ); ?> />
								<label for="form_notification_digest_field_<?php echo $field['id']; ?>"><?php echo esc_html( $field['label'] ); ?></label>
								<br>
							<?php endforeach; ?>
						</div>

						<p>
							<?php if ( $next ): ?><code><?php echo esc_html( $next ); ?></code><?php endif; ?>
							<?php if ( $last ): ?><code><?php echo esc_html( $last ); ?></code><?php endif; ?>
						</p>
					</div>
				</div>
			</div>
		<?php

		return $out;
	}// end func

	/**
	* Describe your function here 
	* 
	* @param string string
	* @return void data
	*/
	private function  _assign_capability( ){
		// register a capability
		$cap = 'manage_digest_bulk_reports';
		$grant = true;

		// get administrator role object
		$admin_role = get_role( 'administrator' );

		// check if admin already have this $cap
		if( ! $admin_role->has_cap( $cap ) ){
			// assign $cap capability to this role 
			$admin_role->add_cap( $cap, $grant );
		}
	
	}//end func

}
