<?php
namespace DIGESTREPORTS;
/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://me@example.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/includes
 * @author     Aakash <me@example.com>
 */
class Gravityforms_Digest_Bulk_Reports_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		\load_plugin_textdomain(
			'gravityforms-digest-bulk-reports',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
