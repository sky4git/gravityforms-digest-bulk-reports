<?php
namespace DIGESTREPORTS;
/**
 * Fired during plugin activation
 *
 * @link       https://me@example.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/includes
 * @author     Aakash <me@example.com>
 */
class Gravityforms_Digest_Bulk_Reports_Activator {

	/**
	 * Reschedule all existing forms
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
		// register a capability
		$cap = 'manage_digest_bulk_reports';
		$grant = true;

		// get administrator role object
		$admin_role = get_role( 'administrator' );
		
		// check if admin already have this $cap
		if( ! $admin_role->has_cap('manage_digest_bulk_reports') ){
			// assign $cap capability to this role 
			$admin_role->add_cap( $custom_cap, $grant );
		}

		$groups = array();
		foreach( \RGFormsModel::get_forms( true ) as $existing_form ) {
			$existing_form = \RGFormsModel::get_form_meta( $existing_form->id );

			if ( !isset( $existing_form['digests'] ) )
				continue;
			if ( !isset( $existing_form['digests']['enable_digest'] ) )
				continue;
			if ( !$existing_form['digests']['enable_digest'] )
				continue;
			if ( !isset( $existing_form['digests']['digest_interval'] ) )
				continue;
			if ( !$existing_form['digests']['digest_interval'] )
				continue;

			$group = isset( $existing_form['digests']['digest_group'] ) ? $existing_form['digests']['digest_group'] : '';
			$interval = $existing_form['digests']['digest_interval'];

			if ( !$group || !isset( $groups["$group.$interval"] ) ) {
				\wp_schedule_event( // Schedule only once
					\apply_filters( 'gf_digest_schedule_next', time() + 3600, $interval ),
					$interval, 'gf_digest_send_notifications', array( intval( $existing_form['id'] ) ) ); 
				$groups["$group.$interval"] = $existing_form['id'];
			}
		}

	}

}