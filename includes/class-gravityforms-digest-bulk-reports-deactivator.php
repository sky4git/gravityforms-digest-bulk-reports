<?php
namespace DIGESTREPORTS;
/**
 * Fired during plugin deactivation
 *
 * @link       https://me@example.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/includes
 * @author     Aakash <me@example.com>
 */
class Gravityforms_Digest_Bulk_Reports_Deactivator {

	/**
	 * Remove all existing schedules
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		$cron = _get_cron_array();

		foreach ( $cron as $timestamp => $schedule ) {
			if ( !isset( $schedule['gf_digest_send_notifications'] ) )
				continue;

			unset( $cron[$timestamp]['gf_digest_send_notifications'] );

			if ( empty( $cron[$timestamp] ) )
				unset( $cron[$timestamp] );
		}

		_set_cron_array( $cron );
		
	}

}