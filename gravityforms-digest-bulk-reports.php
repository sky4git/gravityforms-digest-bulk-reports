<?php
namespace DIGESTREPORTS;
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://me@example.com
 * @since             1.0.0
 * @package           Gravityforms_Digest_Bulk_Reports
 *
 * @wordpress-plugin
 * Plugin Name:       Gravity Forms Digest Bulk Reports
 * Plugin URI:        https://gitlab.com/sky4git/gravityforms-digest-bulk-reports
 * Description:       Send digest notifications about form entries as configured. Simplifies the tracking of new submission for the form.
 * Version:           1.0.0
 * Author:            Aakash
 * Author URI:        https://me@example.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gravityforms-digest-bulk-reports
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'GRAVITYFORMS_DIGEST_BULK_REPORTS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gravityforms-digest-bulk-reports-activator.php
 */
function activate_gravityforms_digest_bulk_reports() {	
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravityforms-digest-bulk-reports-activator.php';
	\DIGESTREPORTS\Gravityforms_Digest_Bulk_Reports_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gravityforms-digest-bulk-reports-deactivator.php
 */
function deactivate_gravityforms_digest_bulk_reports() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gravityforms-digest-bulk-reports-deactivator.php';
	\DIGESTREPORTS\Gravityforms_Digest_Bulk_Reports_Deactivator::deactivate();
}


register_activation_hook( __FILE__,  __NAMESPACE__ . '\activate_gravityforms_digest_bulk_reports' );
register_deactivation_hook( __FILE__,  __NAMESPACE__ .'\deactivate_gravityforms_digest_bulk_reports' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gravityforms-digest-bulk-reports.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gravityforms_digest_bulk_reports() {

	$plugin = new \DIGESTREPORTS\Gravityforms_Digest_Bulk_Reports();
	$plugin->run();

}
\DIGESTREPORTS\run_gravityforms_digest_bulk_reports();
