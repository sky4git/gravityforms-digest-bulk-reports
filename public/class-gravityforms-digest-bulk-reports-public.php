<?php
namespace DIGESTREPORTS;
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://me@example.com
 * @since      1.0.0
 *
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Gravityforms_Digest_Bulk_Reports
 * @subpackage Gravityforms_Digest_Bulk_Reports/public
 * @author     Aakash <me@example.com>
 */
class Gravityforms_Digest_Bulk_Reports_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravityforms_Digest_Bulk_Reports_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravityforms_Digest_Bulk_Reports_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gravityforms-digest-bulk-reports-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gravityforms_Digest_Bulk_Reports_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gravityforms_Digest_Bulk_Reports_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gravityforms-digest-bulk-reports-public.js', array( 'jquery' ), $this->version, false );

	}

	/** 
	 * Sends an e-mail out, good stuff
	 */
	public function send_notifications( $form_id ) {
		$form = \RGFormsModel::get_form_meta( $form_id );

		if ( !$form ) {
			// TODO: Yet, groups will only be sent out in the next schedule
			// TODO: perhaps add a $now = 'group' flag for instant turnaround?
			$this->reschedule_existing();
			return;
		}

		$digest_group = isset( $form['digests']['digest_group'] ) ? sanitize_text_field( $form['digests']['digest_group'] ) : false;
		$digest_interval = isset( $form['digests']['digest_interval'] ) ? sanitize_text_field( $form['digests']['digest_interval'] ) : false;
		$digest_report_always = isset( $form['digests']['digest_report_always'] ) ? sanitize_text_field( $form['digests']['digest_report_always'] ) : false;
		$digest_export_all_fields = isset( $form['digests']['digest_export_all_fields'] ) ? sanitize_text_field( $form['digests']['digest_export_all_fields'] ) : true;
		$digest_export_field_list = isset( $form['digests']['digest_export_field_list'] ) ? sanitize_text_field( $form['digests']['digest_export_field_list'] ) : array();

		$forms = array( $form['id'] => $form );
		if ( $digest_group ) {
			/* We may want to send out a group of forms in one e-mail if possible */
			foreach( \RGFormsModel::get_forms( true ) as $existing_form ) {
				if ( $existing_form->id == $form_id )
					continue; // It is I!
				$existing_form = \RGFormsModel::get_form_meta( $existing_form->id );

				if ( !isset( $existing_form['digests']['enable_digest'] ) )
					continue; // Meh, not interesting
				if ( !isset( $existing_form['digests']['digest_group'] ) )
					continue; // Meh, not interesting
				if ( !isset( $existing_form['digests']['digest_interval'] ) )
					continue; // Meh, not interesting

				if ( $existing_form['digests']['digest_group'] == $digest_group )
					if ( $existing_form['digests']['digest_interval'] == $digest_interval ) {
						$forms[$existing_form['id']]= $existing_form; // Add them all
					}
			}
		}

		$emails = array();

		/* Gather all the leads and update the last_sent counters */
		foreach ( $forms as $i => $form ) {
			$last_sent = isset( $form['digests']['digest_last_sent'] ) ? $form['digests']['digest_last_sent'] : 0;

			/* Retrieve form entries newer than the last sent ID */
			global $wpdb;
			$leads_table = \RGFormsModel::get_lead_table_name();
			$leads = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $leads_table WHERE form_id = %d AND id > %d AND status = 'active';", $form['id'], $last_sent ) );

			if ( !sizeof( $leads ) ) {
				if( !$digest_report_always ) {
					continue; // Nothing to report on
				}
			} else {
				/* Update the reported id counter */
				$form['digests']['digest_last_sent'] = $leads[sizeof($leads) - 1]->id;
			}

			if ( version_compare( \GFCommon::$version, '1.7' ) >= 0 ) {
				/* Seems like 1.7 really messed up the meta structure */
				unset( $form['notifications'] );
				unset( $form['confirmations'] );
			}
			\RGFormsModel::update_form_meta( $form['id'], $form );

			$forms[$i]['leads'] = $leads;

			/* Also make a lookup table of all e-mail addresses to forms */
			foreach ( $form['digests']['digest_emails'] as $email ) {
				if ( !isset( $emails[$email] ) ) $emails[$email] = array();
				$emails[$email] []= $form['id'];
			}
		}

		/* Now, let's try and mail stuff */
		foreach ( $emails as $email => $form_ids ) {

			if ( defined( 'GF_DIGESTS_AS_CSV' ) && GF_DIGESTS_AS_CSV ) {
				/* CSV e-mails */
				$report = 'Report generated at ' . date( 'Y-m-d H:i:s' ) . "\n";
				$csv_attachment = tempnam( sys_get_temp_dir(), '' );
				$csv = fopen( $csv_attachment, 'w' );

				$from = null; $to = null;

				$names = array();
				foreach ( $form_ids as $form_id ) {
					$form = $forms[$form_id];

					$names []= $form['title'];
					/*  fputcsv( $csv, array( 'Form: ' . $form['title'] . ' (#' . $form_id . ')' ) ); */

					$headers = array( 'Date Submitted' );

					if ( $digest_export_all_fields ) {
						foreach ( $form['fields'] as $field )
							if ( $field['label'] ) $headers []= $field['label'];
					} else {
						foreach ( $form['fields'] as $field )
							if ( $field['label'] && in_array( $field['id'], $digest_export_field_list ) ) $headers []= $field['label'];
					}

					fputcsv( $csv, $headers );


					if ( !$form['leads'] ) {
						/* No new entries (but user has opted to receive digests always) */
						fputcsv( $csv, array( __( 'No new entries.', self::$textdomain ) ) );
					} else {
						foreach ( $form['leads'] as $lead ) {
							$data = array();

							$lead_data = \RGFormsModel::get_lead( $lead->id );
							$data []= $lead->date_created;

							if ( !$from )
								$from = $lead->date_created;
							else
								$to = $lead->date_created;

							foreach ( $form['fields'] as $field ) {
								if ( !$field['label'] ) continue;
								if ( !$digest_export_all_fields && !in_array( $field['id'], $digest_export_field_list ) ) continue;
								$raw_data = \RGFormsModel::get_lead_field_value( $lead_data, $field );
								if( !is_array( $raw_data ) ) $data []= $raw_data;
								else $data []= implode( ', ', array_filter( $raw_data ) );
							}

							fputcsv( $csv, $data );
						}
					}

					/* fputcsv( $csv, array( '--' ) );   */
				}

				if ( !$to )
					$to = $from;

				$report .= 'Contains entries from ' . $from . " to $to\n";
				$report .= 'See CSV attachment';

				fclose( $csv );
				copy ($csv_attachment, './wp-content/uploads/data/' . $form_id . '.csv');
				$new_csv_attachment = $csv_attachment . '-' . date( 'YmdHis' ) . '.csv';
				rename( $csv_attachment, $new_csv_attachment );

				\wp_mail(
					$email,
					\apply_filters(
						'gf_digest_email_subject',
						'Form Digest Report (CSV): ' . implode( ', ', $names ),
						$names, array( $from, $to ), $new_csv_attachment ),
					$report, null, array( $new_csv_attachment )
				);

				if ( !defined( 'GF_DIGEST_DOING_TESTS' ) )
					unlink( $new_csv_attachment );
			} else {
				/* Regular e-mails */
				$report = 'Report generated at ' . date( 'Y-m-d H:i:s' ) . "\n";

				$names = array();
				foreach ( $form_ids as $form_id ) {
					$form = $forms[$form_id];
					$report .= "\nForm name:\t" . $form['title'] . "\n";
					$names []= $form['title'];

					$from = null; $to = null;

					if ( !$form['leads'] ) {
						/* No new entries (but user has opted to receive digests always) */
						$report .= __( 'No new entries.', self::$textdomain );
						$report .= "\n--\n";
					} else {
						foreach ( $form['leads'] as $lead ) {
							$lead_data = \RGFormsModel::get_lead( $lead->id );
							$report .= "\n--\n";
							$report .= "submitted on:\t" . $lead->date_created . "\n";

							if ( !$from )
								$from = $lead->date_created;
							else
								$to = $lead->date_created;

							foreach ( $lead_data as $index => $data ) {
								if ( !is_numeric( $index ) || !$data ) continue;
								if ( !$digest_export_all_fields && !in_array(floor($index),$digest_export_field_list) ) continue;
								$field = \RGFormsModel::get_field( $form, $index );
								$report .= "{$field['label']}:\t$data\n";
							}
						}
					}
				}

				if ( !$to )
					$to = $from;

				\wp_mail(
					$email,
					\apply_filters(
						'gf_digest_email_subject',
						'Form Digest Report: ' . implode( ', ', $names ),
						$names, array( $from, $to ), null ),
					$report
				);
			}
		}

		if ( version_compare( \GFCommon::$version, '1.7' ) >= 0 ) {
			/* In 1.7 there seems to be an issue with saving */
			\GFFormsModel::flush_current_forms();
		}
	}// end func

}
