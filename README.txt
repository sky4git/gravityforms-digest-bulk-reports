=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: https://me@example.com
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 4.9.5
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A Gravity Forms Addon for WordPress that generates submission digests for entries.

== Description ==

CSV mode can be enabled by adding define('GF_DIGESTS_AS_CSV', true) in your wp-config.php file.

Visit the Notifications page of a Form for setup. Works well with disabled admin notifications.


For more information visit https://github.com/soulseekah/Gravity-Forms-Digest-Bulk-Reports